const books = [{
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

let list = document.createElement('ul')



books.forEach(element => {

    if (!element.name) {
        console.log('Error no name')
        return
    }

    if (!element.author) {
        console.log('Error no author')
        return
    }
    if (!element.price) {
        console.log('Error no price')
        return
    }

    let tag = document.createElement('li')
    tag.innerHTML = element.author + element.name + element.price

    list.append(tag)

});
document.getElementById('root').append(list)