// Опишите своими словами, что такое метод обьекта
// Метод обьекта это свойство обьекта которое является функцией самого обьекта.
// То есть производит какие-то действия самого обьекта.
// например есть обьект human и он может писать или говорить - это и есть его методы.

'use strict';

// Реализовать функцию для создания объекта "пользователь".Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек(типа Jquery).

// Технические требования:
// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
// При вызове функция должна спросить у вызывающего имя и фамилию.
// Используя данные, введенные пользователем, создать объект newUser со свойствами setFirstName и setLastName.
// Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре(например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser().Вызвать у пользователя функцию getLogin().Вывести в консоль результат выполнения функции.
// Необязательное задание продвинутой сложности:
// Сделать так, чтобы свойства setFirstName и setLastName нельзя было изменять напрямую.Создать функции - сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.

function createNewUser() {
	const newUser = {
		_setFirstName: ``,
		set setFirstName(value) {
			this._setFirstName = value;
		},
		get setFirstName() {
			return this._setFirstName;
		},

		_setLastName: ``,
		set setLastName(value) {
			this._setLastName = value;
		},
		get setLastName() {
			return this._setLastName;
		},

		getLogin() {
			return `${this.setFirstName[0]}${this.setLastName}`.toLowerCase();
		},
	};

	newUser.setFirstName = prompt(`Enter your first name`);
	newUser.setLastName = prompt(`Enter your last name`);

	console.log(newUser.getLogin());

	return newUser;
};

console.log(createNewUser());