const button = document.querySelector(".header__button");
let buttonOpen = document.querySelector(".button--open");
let buttonClose = document.querySelector(".button--close");
let menu = document.querySelector(".menu");

function menuClickHandler({target}){
    if(target.closest(".button--open")){
        buttonOpen.style.cssText += "display: none;";
        buttonClose.style.cssText += "display: block;";
        menu.style.cssText += "display: flex;";
    }else if(target.closest(".button--close")){
        buttonOpen.style.cssText += "display: flex;";
        buttonClose.style.cssText += "display: none;";
        menu.style.cssText += "display: none;";
    }
}

button.addEventListener("click", menuClickHandler);