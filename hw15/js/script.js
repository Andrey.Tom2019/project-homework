// Напишите как вы понимаете рекурсию. Для чего она используется на практике?
// Рекурсия это когда функция при выполненнии своего тела, при определенном условии вызывает самму себя.
// Для чего угодно: из многомерного масива сделать одномерный. И тд...



// Считать с помощью модального окна браузера число, которое введет пользователь.
// С помощью функции посчитать факториал числа, которое ввел пользователь, и вывести его на экран.
// Использовать синтаксис ES6 для работы с перемеными и функциями.
// Необязательное задание продвинутой сложности:
// После ввода данных добавить проверку их корректности. Если пользователь не ввел число, либо при вводе указал не число, - спросить число заново (при этом значением по умолчанию для него должна быть введенная ранее информация). 


`use strict`;

let num;

do {
	num = prompt(`Enter number`);
} while (!num || isNaN(+num) || num.trim() === ``);

let res = 1;
function factorial(num) {

	if (num > 1) {
		res *= num;
		factorial(num - 1);
	}
	return res;
};

alert(factorial(parseInt(num)));