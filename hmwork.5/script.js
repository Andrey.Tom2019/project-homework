'use strict'

/*
Задание 1:
Реализовать функцию для создания объекта "пользователь".
Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре(например, Ivan Kravchenko → ikravchenko).
Создать пользователя с помощью функции createNewUser().Вызвать у пользователя функцию getLogin().Вывести в консоль результат выполнения функции.
*/

// Решение задания 1:


/* 

class CreateNewUser {
	constructor(name, lname) {
		this.firstName = name;
		this.lastName = lname;
	}

	getLogin() {
		return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
	}
}

const newUser = new CreateNewUser(prompt("Введите Ваше имя"), prompt("Введите Вашу фамилию"));

console.log(newUser.getLogin());  

*/



/*
Задание 2:
Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.

Возьмите выполненное задание выше (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
При вызове функция должна спросить у вызывающего дату рождения(текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
Создать метод getAge() который будет возвращать сколько пользователю лет.
Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией(в нижнем регистре) и годом рождения (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
*/

// Решение задания 2:

class CreateNewUser {
    constructor() {
        this.firstName = prompt("Введите Ваше имя");
        this.lastName = prompt("Введите Вашу фамилию");
        this.birthday = prompt("Введите дату Вашего рождения", "текст в формате dd.mm.yyyy");
    };

    getLogin() {
        return `${this.firstName[0].toLowerCase()}.${this.lastName.toLowerCase()}`;
    };

    getAge() {
        const today = new Date();
        const userBirthday = Date.parse(`${this.birthday.slice(6)}-${this.birthday.slice(3, 5)}-${this.birthday.slice(0, 2)}`);
        const age = ((today - userBirthday) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed(0);
        if (age < today) {
            return `Вам ${age - 1} лет`;
        } else {
            return `Вам ${age} лет`;
        }
    };

    getPassword() {
        return `${this.firstName[0].toUpperCase()}${this.lastName.toLocaleLowerCase()}${this.birthday.slice(-4)}`
    };
}

const newUser = new CreateNewUser();
console.log(newUser.getAge());
console.log(newUser.getPassword());



/*
Задание 3:
Реализовать функцию фильтра массива по указанному типу данных.

Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент, за исключением тех, тип которых был передан вторым ргументом.То есть, если передать массив['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет массив[23, null].
*/

// Решение задания 3:

function filterBy(array, dataType) {
    if (Array.isArray(array)) {
        const newArray = array.filter(element => (typeof element) !== dataType);
        console.log(newArray);
    } else {
        alert('Error')
    }
}

const arr = ['Елемент массива', 'string', 25, 56, null, undefined, null, function() {},
    []
];
const filter = prompt("Введіть тип даних, який хочете відфільтрувати:", "undefined, number, bigint, boolean, string, symbol, object, function")
filterBy(arr, filter);