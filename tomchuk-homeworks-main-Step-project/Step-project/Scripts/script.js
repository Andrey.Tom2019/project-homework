"use strict";

const webTabs = document.querySelectorAll(".web-design-titles, .web-design-title");
const webContent = document.querySelectorAll(".services-content");

const listWeb = (el) => {

    document.querySelector(".active-title").classList.remove("active-title");
    el.target.classList.add('active-title');

    webContent.forEach(elem => {
        elem.classList.add("hidden");

        if (
            elem.getAttribute("data-name") ===
            el.target.getAttribute("data-name")) {
            elem.classList.remove("hidden");
            elem.classList.add("active-avatar");
        }
    });

};

webTabs.forEach((e) => {
    e.addEventListener("click", listWeb);

});
const amazingTabs = document.querySelectorAll(" .amazing-tab");
const amazingContent = document.querySelectorAll(".amazing-block");

const listClickHandler = (el) => {

    document.querySelector(".amazing-tab-active").classList.remove("amazing-tab-active");
    el.target.classList.add('amazing-tab-active');

    amazingContent.forEach(elem => {
        elem.classList.add("hidden");

        if (
            elem.getAttribute("data-name") ===
            el.target.getAttribute("data-name")) {
            elem.classList.remove("hidden");
            elem.classList.add("active-avatar");
        }
    });

};

amazingTabs.forEach((e) => {
    e.addEventListener("click", listClickHandler);

});

$("document").ready(function () {
    $("#all").click(function () {
        $(".amazing-item").show(1000);
        $("#all").hide(500);
    })
  });
  const portraitList = [...document.querySelectorAll(".img-block")];
const totalPortrait = [...document.querySelectorAll(".people-img-mini")];
const feedbackText = [...document.querySelectorAll(".feedback-item")];
let position = 0;

const feedbackHandler = (e) => {
    const activePortrait = document.querySelector(".img-block > .active");
    const activeText = document.querySelector(".feedback-list > .active");

    if (e.target.closest(".fas") || e.target.closest(".people-img-mini")) {

        activePortrait.classList.remove("active");
        activeText.classList.remove("active");

        if (e.target.closest(".fa-chevron-right")) {
            position = position === totalPortrait.length - 1 ? 0 : position += 1;
        } else if (e.target.closest(".fa-chevron-left")) {
            position = position === 0 ? totalPortrait.length - 1 : position -= 1;
        }
        if (e.target.closest(".people-img-mini")) {
            position = totalPortrait.indexOf(e.target);
        }
        totalPortrait[position].classList.add("active");

        feedbackText.forEach((el) => {
            if (totalPortrait[position].getAttribute("data-author") ===
                el.getAttribute("data-author")) {
                el.classList.add("active");
            }
        });
    }
};
portraitList.forEach((e) => e.addEventListener("click", feedbackHandler));