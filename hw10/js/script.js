// По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
// Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета Нужно ввести одинаковые значения
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.

`use strict`;

document.addEventListener(`DOMContentLoaded`, () => {

	const password = document.querySelector(`#password`);
	const passwordVerification = document.querySelector(`#password-verification`);
	const passwordEye = password.nextElementSibling;
	const passwordVerificationEye = passwordVerification.nextElementSibling;
	const btnCheck = document.querySelector(`button[type="submit"]`);
	const spanWrongPass = document.createElement(`span`);


	passwordEye.addEventListener(`click`, passwordEyeOnClick);
	passwordVerificationEye.addEventListener(`click`, passwordVerificationEyeOnClick);
	btnCheck.addEventListener(`click`, btnCheckOnClick);

	function passwordEyeOnClick() {
		if (password.getAttribute(`type`) === `password`) {
			password.setAttribute(`type`, `text`);
			passwordEye.className = `fas fa-eye-slash icon-psassword`;
		} else {
			password.setAttribute(`type`, `password`);
			passwordEye.className = `fas fa-eye icon-password`;
		}
	};
	function passwordVerificationEyeOnClick() {
		if (passwordVerification.getAttribute(`type`) === `password`) {
			passwordVerification.setAttribute(`type`, `text`);
			passwordVerificationEye.className = `fas fa-eye-slash icon-psassword`;
		} else {
			passwordVerification.setAttribute(`type`, `password`);
			passwordVerificationEye.className = `fas fa-eye icon-password`;
		}
	};
	function btnCheckOnClick(event) {
		event.preventDefault();
		if (password.value === passwordVerification.value && password.value > 0) {
			spanWrongPass.outerHTML = ``;
			alert(`You are welcome`);
		} else {
			spanWrongPass.textContent = `Нужно ввести одинаковые значения`;
			spanWrongPass.classList.add(`wrong-pass`);
			passwordVerification.after(spanWrongPass);
		}
	};
});