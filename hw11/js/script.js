// Почему для работы с input не рекомендуется использовать события клавиатуры?
// Для input есть вариант прослушки более подходящий, а именно `input`

// Каждая кнопка содержит в себе название клавиши на клавиатуре
// По нажатию указанных клавиш - та кнопка, на которой написана эта буква, должна окрашиваться в синий цвет. При этом, если какая-то другая буква уже ранее была окрашена в синий цвет - она становится черной. Например по нажатию Enter первая кнопка окрашивается в синий цвет. Далее, пользователь нажимает S, и кнопка S окрашивается в синий цвет, а кнопка Enter опять становится черной.

`use strict`

document.addEventListener(`DOMContentLoaded`, () => {

	const btnList = [...document.querySelectorAll(`.btn`)];

	document.addEventListener(`keydown`, onKeyDown);

	function onKeyDown(e) {
		btnList.forEach(btn => {
			if (e.key.toLowerCase() === btn.getAttribute(`data-btn`)) {
				btnList.forEach(btn => {
					btn.classList.remove(`bgc-blue`);
				});
				btn.classList.add(`bgc-blue`);
			}
		});
	}

});