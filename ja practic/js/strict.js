'use strict'

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name() {
        return this.name
    }
    set name(value) {
        return this.name = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salary() {
        return this.salary * 3;
    }
}