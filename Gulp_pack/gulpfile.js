import gulp from "gulp";

import { path } from "./gulp/config/path.js";
import {plugins} from "./gulp/config/plugins.js";
global.app={
    isBuild:process.argv.includes('--build'),
    isDev:!process.argv.includes('--build'),
    path:path,
    gulp:gulp,
    plugins:plugins

}
import { copy } from "./gulp/tasks/copy.js";
import { reset } from "./gulp/tasks/reset.js";
import { html } from "./gulp/tasks/html.js";
import { server } from "./gulp/tasks/server.js";
import { scss } from "./gulp/tasks/scss.js";
import { js } from "./gulp/tasks/js.js";
import { Images } from "./gulp/tasks/Images.js";
import {otfToTtf, ttfToWoff, fontStyle} from "./gulp/tasks/fonts.js"
import {svgSprive} from "./gulp/tasks/svgSprive.js";
 function watcher(){
    gulp.watch(path.watch.files,copy);
    gulp.watch(path.watch.html,html);
    gulp.watch(path.watch.scss,scss);
    gulp.watch(path.watch.js,js);
    gulp.watch(path.watch.images,Images);
    
   
}
export {svgSprive} 
const fonts = gulp.series(otfToTtf, ttfToWoff, fontStyle);
const mainTask = gulp.series(fonts, gulp.parallel(copy, html, scss, js,Images));
const dev = gulp.series(reset,mainTask, gulp.parallel(server, watcher));
// Импорт задач
gulp.task('default', dev);