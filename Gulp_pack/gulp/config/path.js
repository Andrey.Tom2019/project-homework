
import * as nodePath from 'path';

const rootFolder = nodePath.basename(nodePath.resolve());

const buildFolder = './dist';
const srcFolder = './src';
export const path = {

    build:{  
        css:`${buildFolder}/css/`,
        js:`${buildFolder}/Scripts/`,
        html:`${buildFolder}/`,
        images:`${buildFolder}/Images/`,
        files:`${buildFolder}/files/`,
        fonts:`${buildFolder}/fonts/`
        

    },
    src:{
        js:`${srcFolder}/Scripts/app.js`,
        svg:`${srcFolder}/Images/**/*.svg`,
        images:`${srcFolder}/Images/**/*.{jpg,jpeg,png,gif,webp}`,
        scss:`${srcFolder}/scss/style.scss`,
        html:`${srcFolder}/*.html`,
        files:`${srcFolder}/files/**/*.*`,
        svgicons:`${srcFolder}/svgicons/*.svg`,
    },
    watch:{
        js:`${srcFolder}/Scripts/**/*.js`,
        images:`${srcFolder}/Images/**/*.{jpg,jpeg,png,gif,webp}`,
        scss:`${srcFolder}/scss/**/*.scss`,
        html:`${srcFolder}/**/*.html`,
        files:`${srcFolder}/files/**/*.*`,

    },
    clean:buildFolder,
    srcFolder:srcFolder,
    rootFolder:rootFolder
    
};