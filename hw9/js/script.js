// Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

`use strict`;

document.addEventListener(`DOMContentLoaded`, () => {

	const tabsWrapper = document.querySelector(`.tabs`);
	const tabsTitle = [...document.querySelectorAll(`.tabs-title`)];
	const tabsContent = [...document.querySelectorAll(`.tabs-content li`)];

	tabsTitle.forEach(tab => {
		if (tab.classList.contains(`active`)) {
			tabsContent.forEach(tabContent => {
				if (tab.getAttribute(`data-tabs`) === tabContent.getAttribute(`data-tabs`)) {
					tabContent.style.display = `block`;
				}
			});
		}
	});

	tabsWrapper.addEventListener(`click`, tabsWrapperOnClick);

	function tabsWrapperOnClick(event) {
		tabsTitle.forEach(element => {
			element.classList.remove(`active`);
		});
		event.target.classList.add(`active`);

		tabsContent.forEach(element => {
			element.style.display = `none`;
			if (element.getAttribute(`data-tabs`) === event.target.getAttribute(`data-tabs`)) {
				element.style.display = `block`;
			}
		});
	};

});