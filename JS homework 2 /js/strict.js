const books = [{
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const bookList = document.getElementById('book_list');
// console.log(bookList);
// bookList.innerHTML = '';

const list = document.createElement('ul');

books.forEach((book, i) => {
    if (book.author && book.name && book.price) {
        const li = document.createElement('li');
        li.innerHTML = `${book.author} ${book.name} ${book.price}`
        list.appendChild(li);
    } else {
        console.error("ERROR")
    }
});
bookList.appendChild(list);